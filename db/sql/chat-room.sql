/*
SQLyog Ultimate v12.04 (64 bit)
MySQL - 5.5.40-log : Database - heroku_24381df4273e5f2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `chatroom` */

DROP TABLE IF EXISTS `chatroom`;

CREATE TABLE `chatroom` (
  `idChatRoom` int(11) NOT NULL AUTO_INCREMENT,
  `crName` varchar(50) NOT NULL,
  `crDateInserted` datetime DEFAULT NULL,
  `crDateUpdated` datetime DEFAULT NULL,
  `crIsOpen` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idChatRoom`),
  UNIQUE KEY `crName` (`crName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `chatroom` */

insert  into `chatroom`(`idChatRoom`,`crName`,`crDateInserted`,`crDateUpdated`,`crIsOpen`) values (1,'Millenium Falcon Banter...','2017-05-19 20:29:57',NULL,1),(2,'Death Star plans','2017-05-19 20:29:57',NULL,1);

/*Table structure for table `chatroommessage` */

DROP TABLE IF EXISTS `chatroommessage`;

CREATE TABLE `chatroommessage` (
  `idChatRoomMessage` int(11) NOT NULL AUTO_INCREMENT,
  `crmContent` varchar(200) DEFAULT NULL,
  `crmIdUserChatRoom` int(11) NOT NULL,
  `crmDateInserted` datetime DEFAULT NULL,
  `crmDateUpdated` datetime DEFAULT NULL,
  `crmIsVisible` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idChatRoomMessage`),
  KEY `crmIdUserChatRoom` (`crmIdUserChatRoom`),
  CONSTRAINT `ChatRoomMessage_ibfk_1` FOREIGN KEY (`crmIdUserChatRoom`) REFERENCES `userchatroom` (`idUserChatRoom`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `chatroommessage` */

insert  into `chatroommessage`(`idChatRoomMessage`,`crmContent`,`crmIdUserChatRoom`,`crmDateInserted`,`crmDateUpdated`,`crmIsVisible`) values (1,'GRAAARGH!!!!',1,'2017-07-02 15:18:22',NULL,1),(2,'Chewwy.....',2,'2017-07-02 15:18:32',NULL,1),(3,'I find your lack of faith disturbing...',3,'2017-07-02 15:19:11',NULL,1);

/*Table structure for table `chatuser` */

DROP TABLE IF EXISTS `chatuser`;

CREATE TABLE `chatuser` (
  `idChatUser` int(11) NOT NULL AUTO_INCREMENT,
  `cuName` varchar(50) NOT NULL,
  `cuProfileUrl` varchar(100) DEFAULT NULL,
  `cuDateInserted` datetime DEFAULT NULL,
  `cuDateUpdated` datetime DEFAULT NULL,
  `cuIsActive` tinyint(4) DEFAULT '1',
  `cuIsLoggedIn` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idChatUser`),
  UNIQUE KEY `cuName` (`cuName`),
  UNIQUE KEY `cuProfileUrl_UNIQUE` (`cuProfileUrl`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `chatuser` */

insert  into `chatuser`(`idChatUser`,`cuName`,`cuProfileUrl`,`cuDateInserted`,`cuDateUpdated`,`cuIsActive`,`cuIsLoggedIn`) values (1,'Chewbacca','1_chewbacca.jpg','2017-05-19 20:29:56',NULL,1,0),(2,'Han Solo','2_han.jpeg','2017-05-19 20:29:56',NULL,1,0),(3,'Darth Vader','3_darth.jpg','2017-05-19 20:29:56',NULL,1,0),(4,'Emperor','4_emperor.jpg','2017-05-19 20:29:56',NULL,1,0),(5,'Broken R2-D2',NULL,'2017-05-19 20:29:56',NULL,1,0);

/*Table structure for table `errorlog` */

DROP TABLE IF EXISTS `errorlog`;

CREATE TABLE `errorlog` (
  `idErrorLog` int(11) NOT NULL AUTO_INCREMENT,
  `elRequestData` varchar(45) NOT NULL,
  `elMessage` varchar(200) NOT NULL,
  `elUserId` int(11) NOT NULL,
  `elIpAddress` varchar(45) DEFAULT NULL,
  `elDateInserted` datetime DEFAULT NULL,
  `elDateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`idErrorLog`),
  KEY `FK_elIdUser_ChatUser_idChatUser_idx` (`elUserId`),
  CONSTRAINT `FK_elIdUser_ChatUser_idChatUser` FOREIGN KEY (`elUserId`) REFERENCES `chatuser` (`idChatUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

/*Data for the table `errorlog` */

insert  into `errorlog`(`idErrorLog`,`elRequestData`,`elMessage`,`elUserId`,`elIpAddress`,`elDateInserted`,`elDateUpdated`) values (1,'[object Object]','ReferenceError: db is not defined',2,'::ffff:192.168.1.8','2017-06-12 17:14:03',NULL),(2,'[object Object]','Error: The argument passed to findAll must be an options object, use findById if you wish to pass a single primary key value',1,'::ffff:192.168.1.8','2017-06-12 17:16:31',NULL),(3,'[object Object]','ReferenceError: db is not defined',2,'::ffff:192.168.1.8','2017-06-12 17:20:22',NULL),(4,'[object Object]','ReferenceError: db is not defined',3,'::ffff:192.168.1.8','2017-06-12 17:22:28',NULL),(5,'[object Object]','Error: Cannot find module \'../models/\'',1,'::ffff:192.168.1.8','2017-06-12 19:21:42',NULL),(6,'[object Object]','Error: Cannot find module \'../models/\'',1,'::ffff:192.168.1.8','2017-06-12 19:23:38',NULL),(7,'[object Object]','Error: Cannot find module \'../models/\'',2,'::ffff:192.168.1.8','2017-06-12 19:24:22',NULL),(8,'[object Object]','ReferenceError: io is not defined',1,'::ffff:192.168.1.8','2017-06-12 19:35:05',NULL),(9,'[object Object]','ReferenceError: io is not defined',1,'::ffff:192.168.1.8','2017-06-12 19:39:02',NULL),(10,'[object Object]','ReferenceError: io is not defined',1,'::ffff:192.168.1.8','2017-06-12 19:41:57',NULL),(11,'[object Object]','ReferenceError: io is not defined',1,'::ffff:192.168.1.8','2017-06-12 19:43:49',NULL),(12,'[object Object]','ReferenceError: io is not defined',2,'::ffff:192.168.1.8','2017-06-12 19:46:58',NULL),(13,'[object Object]','No rows returned from the database',5,'::ffff:192.168.1.8','2017-06-25 19:43:59',NULL),(14,'[object Object]','No rows returned from the database',1,'::ffff:192.168.1.8','2017-06-26 19:01:29',NULL),(15,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoomMessage.idChatRoomMessage\' in \'field list\'',1,'::ffff:192.168.1.8','2017-06-27 17:47:28',NULL),(16,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoomMessage.idChatRoomMessage\' in \'field list\'',2,'::ffff:192.168.1.8','2017-06-27 17:50:23',NULL),(17,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoomMessage.idChatRoomMessage\' in \'field list\'',2,'::ffff:192.168.1.8','2017-06-27 17:52:21',NULL),(18,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to ChatRoomMessage!',1,'::ffff:192.168.1.8','2017-06-27 18:09:39',NULL),(19,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to ChatRoomMessage!',1,'::ffff:192.168.1.8','2017-06-27 18:11:25',NULL),(20,'[object Object]','Error: UserChatRoom is not associated to ChatRoomMessage!',1,'::ffff:192.168.1.8','2017-06-27 18:27:27',NULL),(21,'[object Object]','Error: UserChatRoom is not associated to ChatRoom!',1,'::ffff:192.168.1.8','2017-06-27 18:38:05',NULL),(22,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatUser.cuIsLoggedIn\' in \'on clause\'',1,'::ffff:192.168.1.8','2017-06-27 18:44:15',NULL),(23,'[object Object]','ReferenceError: result is not defined',1,'::ffff:192.168.1.8','2017-06-27 18:51:34',NULL),(24,'[object Object]','ReferenceError: result is not defined',1,'::ffff:192.168.1.8','2017-06-27 18:53:29',NULL),(25,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.8','2017-06-27 19:12:52',NULL),(26,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to UserChatRoom!',1,'::ffff:192.168.1.70','2017-07-01 12:36:27',NULL),(27,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:36:27',NULL),(28,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to UserChatRoom!',1,'::ffff:192.168.1.70','2017-07-01 12:38:23',NULL),(29,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:38:23',NULL),(30,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to UserChatRoom!',1,'::ffff:192.168.1.70','2017-07-01 12:39:27',NULL),(31,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:39:27',NULL),(32,'[object Object]','Error: UserChatRoom (user_chat_room) is not associated to UserChatRoom!',1,'::ffff:192.168.1.70','2017-07-01 12:41:19',NULL),(33,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:41:20',NULL),(34,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoom.crIsOpen\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:42:02',NULL),(35,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:42:02',NULL),(36,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:43:47',NULL),(37,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:43:47',NULL),(38,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:45:44',NULL),(39,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:45:44',NULL),(40,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:47:45',NULL),(41,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.idChatUser\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:47:45',NULL),(42,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:49:06',NULL),(43,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.cuName\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:49:06',NULL),(44,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',2,'::ffff:192.168.1.70','2017-07-01 12:50:22',NULL),(45,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'chat_user.cuName\' in \'field list\'',2,'::ffff:192.168.1.70','2017-07-01 12:50:22',NULL),(46,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:52:07',NULL),(47,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.cuName\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:52:07',NULL),(48,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',2,'::ffff:192.168.1.70','2017-07-01 12:53:56',NULL),(49,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.cuName\' in \'field list\'',2,'::ffff:192.168.1.70','2017-07-01 12:53:56',NULL),(50,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',2,'::ffff:192.168.1.70','2017-07-01 12:55:52',NULL),(51,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.cuName\' in \'field list\'',2,'::ffff:192.168.1.70','2017-07-01 12:55:52',NULL),(52,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:56:32',NULL),(53,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoomMessage.cuName\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:56:33',NULL),(54,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 12:58:30',NULL),(55,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoomMessage.cuName\' in \'field list\'',1,'::ffff:192.168.1.70','2017-07-01 12:58:30',NULL),(56,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 13:00:49',NULL),(57,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 13:00:49',NULL),(58,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'UserChatRoom.user_id\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 13:04:04',NULL),(59,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 13:04:05',NULL),(60,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoom.crIsOpen\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 13:56:25',NULL),(61,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoom.crIsOpen\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 13:56:42',NULL),(62,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 13:56:42',NULL),(63,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoom.crIsOpen\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 13:58:42',NULL),(64,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 13:58:42',NULL),(65,'[object Object]','SequelizeDatabaseError: ER_BAD_FIELD_ERROR: Unknown column \'ChatRoom.crIsOpen\' in \'where clause\'',1,'::ffff:192.168.1.70','2017-07-01 14:00:42',NULL),(66,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 14:00:42',NULL),(67,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:02:26',NULL),(68,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:02:48',NULL),(69,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 14:02:48',NULL),(70,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:05:13',NULL),(71,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:05:22',NULL),(72,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 14:05:22',NULL),(73,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:06:40',NULL),(74,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:07:35',NULL),(75,'[object Object]','TypeError: val.replace is not a function',1,'::ffff:192.168.1.70','2017-07-01 14:08:19',NULL),(76,'[object Object]','No Chat Room information found in the database',1,'::ffff:192.168.1.70','2017-07-01 14:08:20',NULL),(77,'[object Object]','ReferenceError: roomIsAccessible is not defined',1,'::ffff:192.168.1.70','2017-07-01 14:18:04',NULL),(78,'[object Object]','ReferenceError: roomIsAccessible is not defined',1,'::ffff:192.168.1.70','2017-07-01 14:20:57',NULL),(79,'[object Object]','Error with data from database',1,'::ffff:192.168.1.70','2017-07-01 14:34:07',NULL),(80,'[object Object]','Error with data from database',1,'::ffff:192.168.1.70','2017-07-01 20:18:43',NULL),(81,'[object Object]','Error with data from database',2,'::ffff:192.168.1.70','2017-07-02 11:00:02',NULL);

/*Table structure for table `userchatroom` */

DROP TABLE IF EXISTS `userchatroom`;

CREATE TABLE `userchatroom` (
  `idUserChatRoom` int(11) NOT NULL AUTO_INCREMENT,
  `ucrIdChatRoom` int(11) NOT NULL,
  `ucrIdChatUser` int(11) NOT NULL,
  `ucrDateInserted` datetime DEFAULT NULL,
  `ucrDateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`idUserChatRoom`),
  KEY `ucrIdChatRoom` (`ucrIdChatRoom`),
  KEY `ucrIdChatUser` (`ucrIdChatUser`),
  CONSTRAINT `UserChatRoom_ibfk_1` FOREIGN KEY (`ucrIdChatRoom`) REFERENCES `chatroom` (`idChatRoom`),
  CONSTRAINT `UserChatRoom_ibfk_2` FOREIGN KEY (`ucrIdChatUser`) REFERENCES `chatuser` (`idChatUser`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `userchatroom` */

insert  into `userchatroom`(`idUserChatRoom`,`ucrIdChatRoom`,`ucrIdChatUser`,`ucrDateInserted`,`ucrDateUpdated`) values (1,1,1,'2017-05-19 20:29:57',NULL),(2,1,2,'2017-05-19 20:29:57',NULL),(3,2,3,'2017-05-19 20:29:57',NULL),(4,2,4,'2017-05-19 20:29:57',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
