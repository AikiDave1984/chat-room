var moment = require('moment');

/**
* Return date time in MySQL format
*	Used for building Sequelize queries
*/
var getCurrentTimeInMySQL = function( ) {
	
	var currentTimestamp = moment().unix();//in seconds
		
	var date = new Date();
		
	var year = date.getYear();
	var month = date.getMonth();
	var date = date.getDate();
		
	var dateTimeArray = [year,month,date];
		
	if(moment(dateTimeArray).isDST()){
		var currentDatetime = moment(currentTimestamp*1000).add(1,'hours').format("YYYY-MM-DD HH:mm:ss");
	} else {
		var currentDatetime = moment(currentTimestamp*1000).format("YYYY-MM-DD HH:mm:ss");
	}
	
	return currentDatetime;
}

module.exports = getCurrentTimeInMySQL;