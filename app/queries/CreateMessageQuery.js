var CreateMessageQuery = function( userChatRoomId,message ) {

  var dateTime = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
  
  return {
	crmIdUserChatRoom: userChatRoomId,
	crmContent: message,
	crmDateInserted : dateTime
  }

}

module.exports = CreateMessageQuery;