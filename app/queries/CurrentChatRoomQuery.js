var CurrentChatRoomQuery = function( chatRoomId ) {
  return {
	where : {
	  idChatRoom: chatRoomId
	}
  }
}

module.exports = CurrentChatRoomQuery;