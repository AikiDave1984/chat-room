var ErrorLogInsertQuery = function( req, error, userId ) {

  var requestData = req.body;
  var dateTime = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
  var ipAddress = req.headers[ 'x-forwarded-for' ] || req.connection.remoteAddress;

  return {
	  elRequestData: requestData.toString(),
	  elMessage:error.toString(),
	  elUserId :userId,
	  elIpAddress :ipAddress,
	  elDateInserted: dateTime
  }

}

module.exports = ErrorLogInsertQuery;