"use strict";
var fs = require("fs");

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js")  && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
	  var functionName = file.substring(0,file.length - 3);
	  module.exports[functionName] = require("./" + file);
  });