var AvailableChatRoomsForUserQuery = function( userId ) {
  
  var db = require( '../../models/' );

  return {
	  include :[ {
		  as:'user_chat_room',
		  model: db.UserChatRoom,
		  where: {
			ucrIdChatUser: userId,
			'$ChatRoom.crIsOpen$': 1
		  },
		  required: true
	  } ]
  }
}

module.exports = AvailableChatRoomsForUserQuery;