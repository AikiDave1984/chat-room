var availableUsersQuery = { 
	attributes: [ [ 'idChatUser', 'user_id' ], [ 'cuName', 'username' ] ],
	where: { cuIsLoggedIn: 0 } 
};

module.exports = availableUsersQuery;