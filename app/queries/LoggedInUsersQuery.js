var LoggedInUsersQuery = function( chatRoomId ) {
  var db = require( '../../models/' );
  var attributes = [ [ 'ucrIdChatUser', 'user_id' ]];
  
  return {
		attributes: attributes,
	  include : [ {
		  as:'chat_user',
		  model: db.ChatUser,
		  attributes: [[ 'cuName', 'username' ], [ 'cuProfileUrl', 'profile_url' ] ],
		  where: {
			'$UserChatRoom.ucrIdChatRoom$': chatRoomId,
			'$chat_user.cuIsLoggedIn$': 1
		  },
		  required: true
		} ]
  }
}

module.exports = LoggedInUsersQuery;