var ChatRoomMessagesQuery = function( chatRoomId ) {

  var db = require( '../../models/' );

  var attributes = [['idChatRoomMessage','message_id'],['crmContent','message_content'],['crmDateInserted','date_inserted']];

  return {
	  attributes: attributes,
	  include : [ {

		  model: db.UserChatRoom,
		  as:'user_chat_room',
		  where: {
			'$user_chat_room.ucrIdChatRoom$': chatRoomId
		  },
		  required: true,
		  include: [ {
			attributes:[['idChatUser','user_id'],['cuName','username'],['cuProfileUrl','profile_url']],
			as: 'chat_user',
			model: db.ChatUser,
			required: true
			} ],

		} ],
		order : [
		  [ 'crmDateInserted', 'ASC' ]
		]
  }
}

module.exports = ChatRoomMessagesQuery;