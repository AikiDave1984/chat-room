var CanUserAccessChatRoomQuery = function( userId,chatRoomId ) {
  
  var db = require( '../../models/' );

  return {
	 
		  where: {
			ucrIdChatUser: userId,
			ucrIdChatRoom: chatRoomId,
		  },
		  
		  include: [ {
			as: 'chat_room',
			model: db.ChatRoom,
			required: true,
			where :{
				'$chat_room.crIsOpen$': 1
				}
			} ],
  }
}

module.exports = CanUserAccessChatRoomQuery;