var UserChatRoomIdQuery = function( chatRoomId, userId ) {
  
  return {
	  attributes : [
		  [ 'idUserChatRoom', 'user_chat_room_id' ]
		],
	  where : {
		  '$UserChatRoom.ucrIdChatRoom$': chatRoomId,
		  '$UserChatRoom.ucrIdChatUser$': userId
		}
  }
}

module.exports = UserChatRoomIdQuery;