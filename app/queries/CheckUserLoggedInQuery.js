



var CheckUserLoggedInQuery = function( userId ) {
  
  return {where: ['idChatUser=? and cuIsLoggedIn=1 AND timestamp(cuLastSeen) > timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE)) ', userId]}
			
}

module.exports = CheckUserLoggedInQuery;