/**
* Resolve with user chat room table id integer
*/

var formatUserChatRoomResponse = function(rows){
	
	return new Promise(function(resolve, reject){
		
		if(!rows){
			reject('Error with database');
		} else {
			
			if(rows.length > 0){
				var userChatRoomId = rows[0].dataValues.user_chat_room_id;//primary key for user message/chat room table
			} else {
				var userChatRoomId = false;
			}

			resolve(userChatRoomId );
		}

	});
}


module.exports = formatUserChatRoomResponse ;