/**
* Check user can log in rooms
*	Return Promise holding resolved boolean value
*/
var checkUserAccessRoom = function(rows){
	return new Promise(function(resolve, reject){

		if(!rows){
			reject('Error: problem retrieving data from database')
		} 
		
		if(rows.length === 0){
			resolve(false);
		} else {
			resolve(true);
		}
			
	});

}


module.exports = checkUserAccessRoom;