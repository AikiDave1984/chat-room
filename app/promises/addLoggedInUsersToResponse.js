
/**
* Return Promise holding array of logged in users taken from the sequelize query
*/
var addLoggedInUsersToResponse = function(rows){
	return new Promise(function(resolve, reject){

		if(!rows){
			reject('Error with data from database');
		} else {
			var loggedInUsers = [];
			for(var i =0;i<rows.length;i++){
				loggedInUsers.push(rows[i].dataValues);
			}
			resolve(loggedInUsers);
		}

	});
}

module.exports = addLoggedInUsersToResponse;