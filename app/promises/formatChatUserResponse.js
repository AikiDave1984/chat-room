/**
* Resolve promise with object of chat users available.
*/
var formatChatUserResponse = function(rows){
	return new Promise(function(resolve, reject){

		if(!rows){
			reject('Database Error. File: '+__filename+'. Line No: '+__line);
		} else {
			console.log(rows);
			var chatUsers = [];
			for(var i =0;i<rows.length;i++){
				chatUsers.push(rows[i].dataValues);
			}

			var chatUsersObj = {'chat_users' : chatUsers};

			resolve(chatUsersObj);
		}

	});
}


module.exports = formatChatUserResponse;