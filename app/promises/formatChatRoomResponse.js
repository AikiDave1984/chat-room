/**
* Add available chat rooms for user data to object
*	Resolve promise with this object
*/

var formatChatRoomResponse = function(rows){
	return new Promise(function(resolve, reject){
		
		if(!rows){
			reject('Error: problem retrieving data from database')
		} else {
			var availableChatRooms = {'available_chat_rooms' : rows}
			resolve(availableChatRooms);
		}
			
	});

}


module.exports = formatChatRoomResponse;