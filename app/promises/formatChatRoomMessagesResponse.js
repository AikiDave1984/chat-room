/**
* After query that gets chat room messages for chat room
*	Resolve as an array if query successful
*/

var formatChatRoomMessagesResponse = function(rows){
	return new Promise(function(resolve, reject){
		
		if(!rows){
			reject('Error with getting data');
		} else {
			var chatRoomMessages = [];
			for(var i =0;i<rows.length;i++){
				chatRoomMessages.push(rows[i].dataValues);
			}
			resolve(chatRoomMessages);
		}

	});
}

module.exports = formatChatRoomMessagesResponse;