
/**
* Return chatroom name as resolved value in promise
*/
var formatChatRoomDetailsResponse = function(rows){
	return new Promise(function(resolve, reject){

		if(rows.length === 0){
			reject('No Chat Room information found in the database');
		} else {
			chatRoomName = rows[0].dataValues.crName;
			resolve(chatRoomName);
		}

	});
}


module.exports = formatChatRoomDetailsResponse;