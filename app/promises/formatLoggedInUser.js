/**
* Resolve promise with array of logged in users (objects).
*/
var addLoggedInUsersToResponse = function(rows){
	return new Promise(function(resolve, reject){

		
		if(!rows){
			reject('Error retrieving data');
		} else {
			var loggedInUsers = rows[0].dataValues;
			resolve(loggedInUsers);
		}

	});
}


module.exports = addLoggedInUsersToResponse;