/**
* Socket handler on user logout
*/

var handleUserLeftChatRoom = function(connectedSockets,socket,io){
	
	var userId = connectedSockets[socket.id]['user_id'];
	var chatRoomId = connectedSockets[socket.id]['chat_room_id'];
	
	//Remove user from room
	connectedSockets.splice(socket.id,1);
	
	var responseObj = {};
	
	var dateTime = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
	
	responseObj[ 'user_id' ] = userId;
	responseObj[ 'date_time' ] = dateTime;
	
	console.log( 'User ' + userId + ' has disconnected' );
	
	//send response to other chat users that this user has logged out. (Probably update their DOM accordingly...)
  io.sockets.in( chatRoomId).emit( 'handle_user_left', responseObj );
}

module.exports = handleUserLeftChatRoom;