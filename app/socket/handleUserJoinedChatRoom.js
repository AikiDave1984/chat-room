/**
* Handle user joining chat room by socket
* add to room (literally chat room id)
*/


var handleUserJoinedChatRoom = function(user_obj, connectedSockets,socket){
	var chat_room_id = user_obj.chat_room_id;

	var user_id = user_obj.chat_room_id;
	
	connectedSockets[socket.id] = user_obj;
	
	console.log( 'a user connected to ' + chat_room_id );
  socket.join( chat_room_id);
}

module.exports = handleUserJoinedChatRoom;