/**
* Build socket handler on server
* Receive a posted message from a user
* Save this to the db
* Emit socket to all other sockets listening to this room (all logged in users) as JSON string.
*/

var handleNewMessage = function(message,app,io){
	console.log(message);
	var userId = message.user_id;
	var chatRoomId = message.chat_room_id;
	var messageContent = message.content;
	
	var db = require( '../../models/' );
	var QueryPromises = require( '../promises/' );
	var Queries = require( '../queries/' );
	
	//Sequelize promises
	var userChatRoomIdQuery = Queries.UserChatRoomIdQuery( chatRoomId, userId );
	var formatUserChatRoomResponse = QueryPromises.formatUserChatRoomResponse;
	var formatLoggedInUser = QueryPromises.formatLoggedInUser;

	var responseObj = {};

	db.UserChatRoom.findAll( userChatRoomIdQuery ).then( formatUserChatRoomResponse ).then( function( userChatRoomId ) {
		//insert new message into db
		var newMessageQuery = Queries.CreateMessageQuery( userChatRoomId, messageContent  );
		return db.ChatRoomMessage.create( newMessageQuery);
	} ).then( function( result ) {
		//get full user information of user that posted message over socket
		var thisUserQuery = Queries.ThisUserQuery(userId);
		return db.ChatUser.findAll( thisUserQuery );
	} ).then( formatLoggedInUser ).then( function( chatUser ) {
		//Build response object as json
		var dateTime = new Date().toISOString().slice( 0, 19 ).replace( 'T', ' ' );
		responseObj[ 'user_id' ] = userId;
		responseObj[ 'chat_room_id' ] = chatRoomId;
		responseObj[ 'message' ] = messageContent;
		responseObj[ 'date_time' ] = dateTime;
		responseObj[ 'chatUser' ] = chatUser;

		//Emit message to users in chat room
		io.sockets.in( chatRoomId ).emit( 'handle_chat_message', responseObj );
	} ).catch( function( error ) {
		
		//Error logging
		if ( app.get( 'env' ) === 'development' ) {
			
			//var errorLogInsertquery = Queries.ErrorLogInsertQuery( {}, error, userId );
			//db.ErrorLog.create( errorLogInsertquery );
		}
		console.log( error );
	} );
}

module.exports = handleNewMessage;