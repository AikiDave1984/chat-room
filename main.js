//Load pre-requisites
var express = require( 'express' );
var http = require( 'http' );
var session = require( 'express-session' );
var path = require('path');

var app = express();
var server = http.createServer( app );
var io = require( 'socket.io' ).listen( server );
var fs=require("fs");
var exphbs = require( 'express-handlebars' );
var Promise = require( "bluebird" );

Promise.config({
    cancellation: true,// Enable cancellation
});

//set config variables for deployment to server
var config = require( './config/config' );
config.production['username'] = process.env.user_name;
config.production['password'] = process.env.password;

setupAppConfiguration(app);
loadRoutes(fs);
setupSockets(io);

//Off we go...
server.listen( process.env.PORT || 3000, function() {
  console.log( 'Example app listening on port 3000!' );
} )

module.export = app;

//****************************************Support Functions****************************************************************//

//Should probably move these to an external file...

//Trace stack for error call
Object.defineProperty(global, '__stack', {
  get: function(){
    var orig = Error.prepareStackTrace;
    Error.prepareStackTrace = function(_, stack){ return stack; };
    var err = new Error;
    Error.captureStackTrace(err, arguments.callee);
    var stack = err.stack;
    Error.prepareStackTrace = orig;
    return stack;
  }
});

//Trace line number of error call
Object.defineProperty(global, '__line', {
  get: function(){
    return __stack[1].getLineNumber();
  }
});

//Set up Nodejs app configuration
function setupAppConfiguration(app){
	
	var bodyParser = require( 'body-parser' );
	
	app.engine( 'handlebars',
		exphbs( {
			defaultLayout: 'main'
		} ) );
	app.set( 'view engine', 'handlebars' );

	app.use( bodyParser.json() ); // to support JSON-encoded bodies
	app.use( bodyParser.urlencoded( { // to support URL-encoded bodies
		extended: true
	} ) )

	app.use( session({secret:'blah23213211123213'}) );

	app.use( '/css', express.static( __dirname + '/public/css' ) );
	app.use( '/img', express.static( __dirname + '/public/img' ) );
	app.use( '/bower_components', express.static( __dirname + '/bower_components' ) );

	var options = {
		dotfiles: 'ignore',
		etag: false,
		extensions: [ 'htm', 'html' ],
		index: false
	};

	app.use( express.static( path.join( __dirname, 'public' ), options ) );
	app.set( 'port', process.env.PORT || 3000 );

	app.use( function( req, res, next ) {
		res.set( 'Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0' );
		next();
	} );
}

//load app routes
function loadRoutes(fs){
	var routePath="./routes/";

	fs.readdirSync(routePath).forEach(function(file) {
			var route=routePath+file;
			require(route)(app,io);
	});
}

//Initiate sockets for user
function setupSockets(io){
	var handleUserJoinedChatRoom = require( './app/socket/handleUserJoinedChatRoom' );
	var handleUserLeftChatRoom = require( './app/socket/handleUserLeftChatRoom' );
	var handleNewMessage = require( './app/socket/handleNewMessage' );
  var connectedSockets = [];
	io.sockets.on( 'connection', function( socket ) {
		
		socket.on( 'chatroom', function( userObj ) {
			handleUserJoinedChatRoom(userObj, connectedSockets,socket)
		} );
		
		socket.on('disconnect', function() {
			handleUserLeftChatRoom(connectedSockets,socket,io);
		});

		socket.on( 'post_new_message', function( message ) {
			handleNewMessage(message,app,io);
		} );
	} );
}