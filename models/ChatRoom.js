module.exports = function(sequelize, DataType){
	var ChatRoom = sequelize.define("ChatRoom",{
		idChatRoom: {
			type: DataType.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		crName: {
			type: DataType.STRING,
			allowNull: false
		},
		crDateInserted: {
			type: DataType.DATE
		},
		crDateUpdated: {
			type: DataType.DATE
		},
		crIsOpen: {
			type: DataType.INTEGER,
			defaultValue: 0
		}

	}, {
      classMethods: {
        //models/index.js calls this function
        associate: function(models) {
          ChatRoom.hasMany(models.UserChatRoom, {foreignKey: 'ucrIdChatRoom',as:'user_chat_room'});
        }
      }
    })

	return ChatRoom;

}