module.exports = function(sequelize, DataType){
	var ChatRoomMessage = sequelize.define("ChatRoomMessage",{
		idChatRoomMessage: {
			type: DataType.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		crmContent: {
			type: DataType.STRING
		},
		crmIdUserChatRoom: {
			type: DataType.INTEGER
		},
		crmDateInserted: {
			type: DataType.DATE
		},
		crmDateUpdated: {
			type: DataType.DATE
		},
		crmIsVisible:{
			type: DataType.INTEGER,
			defaultValue: 1
		}

	}, {
      classMethods: {
        //models/index.js calls this function
        associate: function(models) {
          ChatRoomMessage.belongsTo(models.UserChatRoom, {foreignKey: 'crmIdUserChatRoom',as: 'user_chat_room'});
        }
      }
    })

	return ChatRoomMessage;

}