module.exports = function(sequelize, DataType){
	var UserChatRoom = sequelize.define("UserChatRoom",{
		idUserChatRoom: {
			type: DataType.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		ucrIdChatUser: {
			type: DataType.INTEGER
		},
		ucrIdChatRoom: {
			type: DataType.INTEGER
		},
		ucrDateInserted: {
			type: DataType.DATE
		},
		ucrDateUpdated: {
			type: DataType.DATE
		}

	}, {
      classMethods: {
        //models/index.js calls this function
        associate: function(models) {
          UserChatRoom.belongsTo(models.ChatRoom, {foreignKey: 'ucrIdChatRoom',as:'chat_room'});
          UserChatRoom.belongsTo(models.ChatUser, {foreignKey: 'ucrIdChatUser',as:'chat_user'});
					UserChatRoom.hasMany(models.ChatRoomMessage, {foreignKey: 'crmIdUserChatRoom',as:'chat_room_message'});
        }
      }
    })
	
	

	return UserChatRoom;

}