module.exports = function(sequelize, DataType){
	var ChatUser = sequelize.define("ChatUser",{
		idChatUser: {
			type: DataType.INTEGER,
			autoIncrement: true,
			primaryKey: true,
		},
		cuName: {
			type: DataType.STRING,
			allowNull: false,
			unique: true
		},
		cuProfileUrl: {
			type: DataType.STRING,
			unique: true
		},
		cuDateInserted: {
			type: DataType.DATE
		},
		cuDateUpdated: {
			type: DataType.DATE
		},
		cuIsActive: {
			type: DataType.INTEGER,
			defaultValue: 1
		},
		cuIsLoggedIn: {
			type: DataType.INTEGER,
			defaultValue: 0
		},
		cuLastSeen: {
			type: DataType.DATE
		},
	}, {
      classMethods: {
        //models/index.js calls this function
        associate: function(models) {
          ChatUser.hasMany(models.UserChatRoom, {foreignKey: 'ucrIdChatUser',as:'user_chat_room'});
					ChatUser.hasMany(models.ErrorLog, {foreignKey: 'elIdChatUser',as:'error_log'});
        }
      }
    })

	return ChatUser;

}
