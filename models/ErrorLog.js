module.exports = function(sequelize, DataType){
	var ErrorLog = sequelize.define("ErrorLog",{
		idErrorLog: {
			type: DataType.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		elRequestData: {
			type: DataType.STRING
		},
		elMessage: {
			type: DataType.STRING
		},
		elUserId: {
			type: DataType.INTEGER
		},
		elIpAddress: {
			type: DataType.STRING
		},
		elDateInserted: {
			type: DataType.DATE
		},
		elDateUpdated: {
			type: DataType.DATE
		}

	}, {
      classMethods: {
        //models/index.js calls this function
        associate: function(models) {
          ErrorLog.belongsTo(models.ChatUser, {foreignKey: 'elUserId',as:'chat_user'});
        }
      }
    })

	return ErrorLog;

}