# Star Wars Chat Room

## Synopsis

This is a demo chat application allowing messages to be stored in a MySQL database.  Based on Star Wars!!! 

This enhances the Chatroom example provided on [socket.io](http://socket.io) to allow users to save messages.  It was used as a learning experience in NodeJs and various libraries.  In particular I wanted to learn how Promises worked
and how to integrate these with code performing MySQL classes

## Installation

Git clone the application.  Set up a MySQL server and update the `config/config.json` file with the connection settings.  You should run `db/sql/chat-room.sql` to set up 
the database on an instance somewhere.  

You'll need to install Nodejs and steps can be found on the [nodejs](https://nodejs.org/en/) website.

You should retrieve the server libraries by running:
```
sudo npm install
```

Similarly for Bower its:
```
bower install
```
 to get the client side libraries.

## Libraries of Interest

These are the important libraries used in development.

 * [Sequelize](http://docs.sequelizejs.com/) - ORM 
 * [Bluebird](http://bluebirdjs.com/docs/getting-started.html) - Promise Library
 * [socket.io](http://socket.io) - Realtime socket connection library
 * [Handlebars](http://handlebarsjs.com/) - view template engine for NodeJS

## Contributors

David Burgess.
