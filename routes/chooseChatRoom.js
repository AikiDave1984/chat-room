var db = require( '../models/' );
var QueryPromises = require( '../app/promises/' );
var Queries = require( '../app/queries/' );
var getCurrentTimeMySQL = require( '../app/helper/getCurrentTimeInMySQL' );

module.exports = function(app){
	/**
	* Load page so user can select chat room
	* Perform checks that user can see available chat rooms
	*/
	app.all( '/chooseChatRoom', function( req, res ) {
		
		var sess = req.session;

		//Sequlize query objects
		var currentDateTime = getCurrentTimeMySQL();
		var formatChatRoomResponse = QueryPromises.formatChatRoomResponse;
		var formatLoggedInUser = QueryPromises.formatLoggedInUser;
		var availableChatRoomsForUserQuery = Queries.AvailableChatRoomsForUserQuery(userId);
		var checkUserLoggedInQuery = Queries.CheckUserLoggedInQuery(userId);
		
		var responseObj = {};
		
		//user has logged in before and redirected to this page - perhaps with browser
		if ( sess.isLoggedIn ) {
			var userId = sess.isLoggedIn;
						
			var p =db.ChatUser.findAll( checkUserLoggedInQuery).then(function(rows){
				//check user can access page
				if(rows.length === 0){
					p.cancel();
					//log out if they can't
					res.redirect('/logout');
				} else {
					//update last seen info in database
					return db.ChatUser.update( {cuLastSeen:currentDateTime}, {where: {idChatUser: userId	}});
				}
			}).then(function(){
				//renew expiry time to 30mins in session
				sess.isLoggedIn = userId;
				sess.cookie.maxAge =  30 * 60 * 1000;
				return db.ChatRoom.findAll( availableChatRoomsForUserQuery );
			}).then( formatChatRoomResponse ).then( function( result ) {
				//add available chat rooms to view object
				responseObj[ 'available_chat_rooms' ] = result[ 'available_chat_rooms' ];
				var thisUserQuery = Queries.ThisUserQuery( userId );
				return db.ChatUser.findAll( thisUserQuery );
			} ).then( formatLoggedInUser ).then( function( chatUser ) {
				//load user info
				responseObj[ 'chatUser' ] = chatUser;
				res.render( 'available_chatrooms', responseObj );
			} ).catch( function( error ) {
				//Error handling
				if ( app.get( 'env' ) === 'development' ) {
					var errorLogInsertquery = Queries.ErrorLogInsertQuery( req, error, userId );
					db.ErrorLog.create( errorLogInsertquery );
				}
				console.log( error );
			} );
		} else if ( typeof( req.body.user_id ) !== "undefined" ) {
			
			//User has just logged in
			var userId = req.body.user_id;
			var checkUserLoggedInQuery = Queries.CheckUserLoggedInQuery(userId);
			
			var p =db.ChatUser.findAll( checkUserLoggedInQuery).then(function(rows){
				//user has already logged in - error
				if(rows.length === 1){
					p.cancel();
					res.redirect('/logout');
				} else {
					//update logged in field in database
					return 	db.ChatUser.update( {cuIsLoggedIn: 1,cuLastSeen:currentDateTime}, {where: {idChatUser: userId	}});
				}
			}).then( function( result ) {
				//update session time with an expiry of 30mins
				sess.isLoggedIn = userId;
				sess.cookie.maxAge =  30 * 60 * 1000;
				var availableChatRoomsForUserQuery = Queries.AvailableChatRoomsForUserQuery(userId);
				return db.ChatRoom.findAll( availableChatRoomsForUserQuery );

			} ).then( formatChatRoomResponse ).then( function( result ) {
				//add available chat rooms to view object
				responseObj[ 'available_chat_rooms' ] = result[ 'available_chat_rooms' ]
				var thisUserQuery =  Queries.ThisUserQuery( userId );
				return db.ChatUser.findAll( thisUserQuery );

			} ).then( formatLoggedInUser ).then( function( chatUser ) {
				
				//add user info to view object
				responseObj[ 'chatUser' ] = chatUser;
				//Load view
				res.render( 'available_chatrooms', responseObj );
			} ).catch( function( error ) {
				//Error handling
				if ( app.get( 'env' ) === 'development' ) {
					var errorLogInsertquery = Queries.ErrorLogInsertQuery( req, error, userId );
					db.ErrorLog.create( errorLogInsertquery );
				}
				var error_object = {'error_title': error.name,'error_message':error.toString()}
				res.render( 'error', error_object );
				console.log( error );
			} );

		} else {
			//redirect to log in page
			res.redirect('/');
		}

	} );
}