var db = require( '../models/' );
var QueryPromises = require( '../app/promises/' );
var Queries = require( '../app/queries/' );
var getCurrentTimeMySQL = require( '../app/helper/getCurrentTimeInMySQL' );

module.exports = function(app){

	/**
	*  Mock log in page - no passwords are required.
	*	 User chooses a character avatar from page
	*  
	*/
	app.get( '/', function( req, res ) {

		var sess = req.session;

		if ( sess.isLoggedIn ) {
			//redirect to choose chat room page if user has already logged in 
			userId = sess.isLoggedIn;
			var moment = require('moment');
			var currentDateTime = getCurrentTimeMySQL();
			
			db.ChatUser.update( {cuLastSeen:currentDateTime}, {where: {idChatUser: userId	}}).then(function(){
				//update session expiry time
				sess.isLoggedIn = userId;
				sess.cookie.maxAge =  30 * 60 * 1000;
				res.redirect( '/chooseChatRoom' );
			});
		} else {
			//Sequelize query objects
			var availableUsersQuery = Queries.AvailableUsersQuery;
			var formatChatUserResponse = QueryPromises.formatChatUserResponse;
			
			db.ChatUser.update( {cuLastSeen:null,cuIsLoggedIn:0}, {where: ['cuIsLoggedIn = 1 AND timestamp(cuLastSeen) < timestamp(DATE_SUB(NOW(), INTERVAL 30 MINUTE))']}).then(function(){
				//log out users with no activity after 30mins
				return db.ChatUser.findAll( availableUsersQuery );
			}).then( QueryPromises.formatChatUserResponse ).then( function( result ) {
				//load log in page
				res.render( 'chatusers', result );
			} ).catch( function( error ) {
				//Error handling
				if ( app.get( 'env' ) === 'development' ) {
					var errorLogInsertquery = Queries.ErrorLogInsertQuery( req, error );
					db.ErrorLog.create( errorLogInsertquery )
				}
				
				var error_object = {'error_title': error.name,'error_message':error.toString()}
				res.render( 'error', error_object );
				console.log( error );
			} );
		}
	} );
}