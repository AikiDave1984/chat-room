module.exports = function(app){
	app.get( '/logout', function( req, res ) {
		/**
		* User wishes to log out
		*	Destroy session and update db fields
		*/
		var db = require( '../models/' );

		var sess = req.session;
		var userId = sess.isLoggedIn;

		//Update db
		db.ChatUser.update( {
			cuIsLoggedIn: 0,
			cuLastSeen: null
		}, {
			where: {
				idChatUser: userId
			}
		} ).then( function( result ) {
			
			//destroy sessions
			req.session.destroy();
			res.redirect( '/' );
		} ).catch( function( error ) {
			//Handle errors
				if ( app.get( 'env' ) === 'development' ) {
					var errorLogInsertquery = Queries.ErrorLogInsertQuery( req, error, userId );
					db.ErrorLog.create( errorLogInsertquery );
				}
				var error_object = {'error_title': error.name,'error_message':error.toString()}
				res.render( 'error', error_object );
				console.log( error );
			} );
	} );
}