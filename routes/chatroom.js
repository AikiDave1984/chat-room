var db = require( '../models/' );
var QueryPromises = require( '../app/promises/' );
var Queries = require( '../app/queries/' );
var getCurrentTimeMySQL = require( '../app/helper/getCurrentTimeInMySQL' );


module.exports = function(app, io){

	/**
	*  User has selected avatar and chosen log in room
	*	 Verify these details in the session and database
	*	 Gather all data for the room and then load the chatroom view
	*	 On completion sockets are established to see real-time chat messages.
	*/
	app.get( '/ChatRoom', function( req, res ) {
		var chatRoomId = parseInt(req.param('chatRoomId'));
		
		var sess = req.session;
		var userId = sess.isLoggedIn;
		
		//redirect to home page if user isn't logged in
		if ( !sess.isLoggedIn ) {		
			res.redirect( '/' );
			return;
		}
		
		//Get sequelize query objects
		var loggedInUsersQuery = Queries.LoggedInUsersQuery( chatRoomId );
		var chatRoomMessagesQuery = Queries.ChatRoomMessagesQuery( chatRoomId );
		var thisUserQuery = Queries.ThisUserQuery( userId );
		var currentChatRoomQuery = Queries.CurrentChatRoomQuery( chatRoomId  );
		var canUserAccessChatRoomQuery = Queries.CanUserAccessChatRoomQuery( userId,chatRoomId  );
		var checkUserLoggedInQuery = Queries.CheckUserLoggedInQuery(userId);

		var addLoggedInUsersToResponse = QueryPromises.addLoggedInUsersToResponse;
		var formatChatRoomMessagesResponse = QueryPromises.formatChatRoomMessagesResponse;
		var formatLoggedInUser = QueryPromises.formatLoggedInUser;
		var formatChatRoomDetailsResponse = QueryPromises.formatChatRoomDetailsResponse;
		var checkUserAccessRoom = QueryPromises.checkUserAccessRoom;
		var currentDateTime = getCurrentTimeMySQL();
		
		//object holding model data used to populate the view
		var data = {};

		data[ 'chat_room_id' ] = chatRoomId;
		data[ 'user_id' ] = userId

		//See if user is logged in
		var p =db.ChatUser.findAll( checkUserLoggedInQuery).then(function(rows){
			
			if(rows.length === 0){
				//no logged in user
				p.cancel();
				res.redirect('/logout');
			} else {
				//update last seen time - renew expiry time
				return db.ChatUser.update( {cuLastSeen:currentDateTime}, {where: {idChatUser: userId	}});
			}
		}).then(function(){
			//set user log in time in session to 30mins
			sess.isLoggedIn = userId;
			sess.cookie.maxAge =  30 * 60 * 1000;
			return db.UserChatRoom.findAll(canUserAccessChatRoomQuery);
			
		}).then( checkUserAccessRoom ).then(function(roomIsAccessible){
			//see if user has permissions to access chat room
			if(!roomIsAccessible){
				res.redirect( '/' );
				return;
			}
			return db.UserChatRoom.findAll( loggedInUsersQuery );
		}).then( addLoggedInUsersToResponse ).then( function( chatUserArray ) {
			//get other users logged in
			data[ 'chat_users' ] = chatUserArray;
			return db.ChatRoomMessage.findAll( chatRoomMessagesQuery )
		} ).then( formatChatRoomMessagesResponse ).then( function( chatRoomMessages ) {
			//retrieve existing chat room messages from the database
			data[ 'available_chat_room_messages' ] = chatRoomMessages;
			return db.ChatRoom.findAll( currentChatRoomQuery )
		} ).then( formatChatRoomDetailsResponse ).then( function( chatRoomName ) {
			//get chat room name from database
			data[ 'chat_room_name' ] = chatRoomName ;
			return db.ChatUser.findAll( thisUserQuery )
		} ).then( formatLoggedInUser ).then( function( chatUser ) {
			//get user information
			data[ 'chatUser' ] = chatUser;
			//render the view with the data
			res.render( 'chatroom', data );
			//set up socket to handle user joining chat room
			io.sockets.in( chatRoomId ).emit( 'handle_logged_in_user', data[ 'chatUser' ] );
		} ).catch( function( error ) {
			//Error handling
			if ( app.get( 'env' ) === 'development' ) {
				var errorLogInsertquery = Queries.ErrorLogInsertQuery( req, error, userId );
				db.ErrorLog.create( errorLogInsertquery );
			}
				var error_object = {'error_title': error.name,'error_message':error.toString()}
				res.render( 'error', error_object );
				console.log( error );
		} );

	} );
}